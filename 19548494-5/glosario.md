## Glosario de Términos Asociados con Control de Versiones ##

* **Control de Versiones:** Es un software diseñado para administrar los ficheros, código fuente 
y/o  documentación  y  los  cambios  realizados  a  dichos,  manteniendo  un  historial  de 
cambios  realizados  a  cada  archivo.  De  esta  manera  se  mantiene  un  control  sobre  las 
ediciones a los distintos módulos de un proyecto. *(Fuente: ecured.cu)*
* **Control  de  versiones  distribuido:**  Es  un  sistema  de  control  de  versiones  en  donde  el 
proyecto  no  se  aloja  en  un  repositorio  central,  en  su  lugar  cada  desarrollador  tiene  su 
propia  copia  del  proyecto  en  un  repositorio  local.  Esto  permite  que  cada  desarrollador 
pueda  editar  su  propia  versión  del  proyecto  sin  interferir  con  el  trabajo  de  los  demás 
integrantes del equipo de trabajo. *(Fuente: ecured.cu)*
* **Repositorio  Remoto  y  Repositorio  Local:**  Un  repositorio  remoto  es  una  versión  del 
proyecto  que  se  encuentra  alojada  en  algún  servidor  o  sitio  de  la  red  de  trabajo  o  en 
internet. Al trabajar con repositorios remotos se debe acudir a los comandos push y pull 
para actualizar el repo remoto con los cambios realizados en tu repo local y para actualizar 
tu versión local con los más recientes cambios hechos al repo remoto respectivamente.
Un repositorio local corresponde a una versión del proyecto alojada en una estación de 
trabajo  de  un  desarrollador  *(ejemplo:  un  computador  personal)*.  Esto  permite  trabajar 
realizando cualquier tipo de cambios sin interferir el trabajo de los demás, enviando los 
cambios a un repo remoto cuando los cambios estén realizados. *(Fuente: git-scm.com)*
* **Copia de trabajo:** Mediante el comando git clone se puede sacar una copia de trabajo de la 
última versión de un proyecto, de esta manera creamos un repositorio local sobre el cual 
trabajar. *(Fuente: git-scm.com)*
* **Área de Preparación:** El área de preparación almacena información acerca de qué cambios 
en el repo irán en el próximo commit a realizar. *(Fuente: git-scm.com)*
* **Preparar  Cambios:**  Marcar  archivos  modificados  de  manera  que  figuren  en  el  área  de 
preparación. *(Fuente: git-scm.com)*
* **Confirmar  cambios:**  Mediante  la  acción  de  confirmar  cambios  *(commit  changes)*  las 
ediciones realizadas a los archivos rastreados por git se guardan en la hebra o branch. *(Fuente: git-scm.com)*
* **Commit:** El comando Commit se usa para guardar cambios en el repositorio local sobre  el 
cual se trabaja, se debe especificar mediante un comentario cuales son exactamente los 
cambios realizados. *(Fuente: git-tower.com)*
* **Clone:**  Mediante  el  comando  clone  es  posible  copiar  un  proyecto  de  un  repo  remoto, 
creando una copia de todos los archivos  del mismo para poder hacer cambios de manera 
local. *(Fuente: git-scm.com)*
* **Pull:**  Con  el  comando  pull  los  posibles  cambios  realizados  en  el  repo  remoto  pasan  a 
actualizar  el  repo  local,  manteniendo  todas  las  versiones  del  proyecto  con  las  últimas 
actualizaciones. *(Fuente: git-scm.com)*
* **Push:** Mediante el comando push los cambios realizados en un repo local son enviados al 
repo remoto para actualizarlo. *(Fuente: git-scm.com)* 
* **Fetch:** Fetch cumple la misma función que git pull, con la diferencia de que no integra  los 
cambios del repo remoto de manera automática, esto se debe hacer manualmente con  el 
comando git merge.*(Fuente: stackoverflow.com)*
* **Merge:** Cuando se desean combinar cambios realizados en distintas ramas de un proyecto 
se utiliza el comando merge para fusionar ambas ramas en un nuevo commit. *(Fuente: gitscm.com)*
* **Status:**  Es  un  comando  que  facilita  información  sobre  el  trabajo  que  se  ha  estado 
realizando. Es un comando muy útil cuando por alguna razón se olvida lo que se estaba 
haciendo en la consola de git o si se debe dejar de lado por un tiempo prolongado; por eso 
mismo es recomendado usar el comando git status para “ponerse al día” o corroborar que 
los cambios  hechos se realizaron correctamente como un merge o ver la lista de archivos 
en stage index. *(Fuente: git-scm.com)*
* **Log:**  Mediante  el  comando  log  podemos  acceder  a  distinta  información,  en  diversos 
formatos y con varios grados de detalle. Es posible visualizar las líneas nuevas y borradas 
en un archivo de código; ver los commits de un proyecto con su id (SHA); incluso graficar 
en la consola las ramas del repo. *(Fuente: git-scm.com)*
* **Checkout:**  Usando  el  comando  checkout  es  posible  moverse  entre  las  ramas  de  un 
repositorio. *(Fuente: git-scm.com)*
* **Rama:** En un repositorio cada commit posee un puntero al commit siguiente, resultando 
así este desarrollo incremental en el cual se basa el control de versiones. Un commit padre 
puede  apuntar  a  más  de  un  commit  hijo,  generándose  así  un  desarrollo  en  paralelo  de 
versiones  del  proyecto,  donde  cada  hijo  puede  ser  trabajado  de  manera  aislada.  Este 
desarrollo en paralelo se le conoce como ramas del repositorio. *(Fuente: git-scm.com)*
* **Tag:** Con tag es posible añadir etiquetas en algún punto de la historia del desarrollo, por 
ejemplo, marcar eventos importantes como podría ser  una versión 1.0 o 2.0.  *(Fuente: git-scm.com)*